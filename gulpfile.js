var gulp = require("gulp");
var sass = require("gulp-sass");
var pug = require("gulp-pug");
var watch = require("gulp-watch");
var plumber = require("gulp-plumber");
var concat  = require('gulp-concat');
var browserSync = require("browser-sync");
var notify = require("gulp-notify");
var runSequence = require('run-sequence');
var del = require('del');
var path = require('path');
var changed  = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var svgmin = require('gulp-svgmin');
var pngquant = require('imagemin-pngquant');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');


gulp.task('default', function(callback) {
  return runSequence(
    'clean',
    [
      'fonts',
      'sass',
      'pug',
      'php',
      'js',
      'plugin',
      'favicon',
      'imagemin',
      'svgmin',
      'browser-sync',
      'watch'
    ],
    callback
  );
});


gulp.task("clean", function() {
  return del(["./dist"]);
});


//sassの監視をして変換処理させる
gulp.task('watch', () => {
    watch(['./sass/**/*.scss'], () => {
        gulp.start(['sass']);
    });
    watch(['./pug/**/*.pug'], () =>  {
      gulp.start(['pug']);
    });
    watch(['./php/*.php'], () =>  {
      gulp.start(['php']);
    });
    watch(["./resources/**/*.json"], () => {
      gulp.start(["pug"]);
    });
    watch(["./js/**/*.js"], () => {
      gulp.start(["js"]);
    });
    watch(["./images/*.+(jpg|jpeg|png|gif)"], () => {
      gulp.start(["imagemin"]);
    });
    watch(["./images/*.+(svg)"], () => {
      gulp.start(["svgmin"]);
    });
});

//ブラウザ表示
gulp.task('browser-sync', () => {
    browserSync({
        server: {
            baseDir: "./dist"   //サーバとなるrootディレクトリ
        }
    });
    watch("./js/**/*.js",     ['reload']);
    watch("./*.html",         ['reload']);
});

//font dest
gulp.task("fonts", () => {
  gulp.src("./Fonts/*.+(otf|ttf|ttc|)")
      .pipe(gulp.dest("./dist/Fonts"))
});

//sassをcssに変換
gulp.task("sass", () => {
    gulp.src("./sass/**/*.scss")
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass())
        .pipe(cleanCSS())
        .pipe(gulp.dest("./dist/css"))
        //reloadせずにinjectする
        .pipe(browserSync.stream())
});

//pugをhtmlに変換
gulp.task("pug", () => {
  gulp.src("./pug/*.pug")
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(pug(
      Object.assign({
        pretty: true
      },
        {
          data: (() => {
            delete require.cache[path.resolve("./resources/bundle.js")];
            return Object.assign({}, {
              resources: require(path.resolve("./resources/bundle.js")).default,
            });
          })(),
        },
      )
    ))
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream())
});
//php
gulp.task("php", () => {
  gulp.src("./php/*.php")
      .pipe(gulp.dest("./dist/"))
});

//js 結合
gulp.task('js', () => {
   gulp.src('./js/src/*.js')
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(concat('script.js'))
    .pipe(uglify('script.js'))
    .pipe(gulp.dest('./dist/js'));

    gulp.src('./js/config/*js')
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(gulp.dest('./dist/js/config'));
});


//画像の圧縮
gulp.task('imagemin', function(){
    gulp.src("./images/*.+(jpg|jpeg|png|gif|pdf)")
    .pipe(imagemin(
      [
        pngquant()
      ],
      {
        interlaced: false,
        optimizationLevel: 3,
        colors:180
      }
    ))
    .pipe(gulp.dest("./dist/images"));
});
//画像 svgの圧縮
gulp.task('svgmin', function(){
    gulp.src("./images/*.+(svg)")
    .pipe(svgmin())
    .pipe(gulp.dest("./dist/images"));
});

//プラグインの出力
gulp.task("plugin", () => {
  gulp.src("./plugin/**")
      .pipe(gulp.dest("./dist/plugin"))
});

//favicon
gulp.task("favicon", () => {
  gulp.src("./favicon/**.ico")
      .pipe(gulp.dest("./dist/favicon"))
});

//ブラウザリロード処理
gulp.task('reload', () => {
    browserSync.reload();
});
